# Description:
#   Allows hubot to file a issue on a particular repo given the name of the repo
#   the desired title, and description of the issue to be filed.
#
# Author:
#   Meet Mangukiya (@meetmangukiya)
#   Satwik Kansal (@satwikkansal)
#
# Commands:
#   hubot new issue - Creates a GitHub issue given the title and repository.
#
# Configuration:
#   HUBOT_GITHUB_OAUTH
#   HUBOT_GITLAB_OAUTH
#   HUBOT_GITHUB_ORGNAME
#   HUBOT_NAME

githubAccessToken = process.env.HUBOT_GITHUB_OAUTH
gitlabAccessToken = process.env.HUBOT_GITLAB_OAUTH
username = process.env.HUBOT_GITHUB_ORGNAME
gitterToken = process.env.HUBOT_GITTER2_TOKEN

# Project's ID is required to create a issue using Gitlab's api
GITLAB_PROJECT_IDS = {
  "cobot" : "1630893",
  "coala-utils" : "1259821",
  "dependency_management" :  "2180762",
  "cib" : "1490040",
}

module.exports = (robot) ->
  robot.respond /(?:new|file) issue$/i, (res) ->
    res.send "The correct syntax for filing an issue is\n" +
             "```\n#{robot.name} file issue <repository> <title>\n" +
             "[description]\n```"

  robot.respond /(?:new|file) issue ([\w-]+?)(?: |\n)(.+?)(?:$|\n((?:.|\n)*))/i, (res) ->
    repoName = res.match[1]
    issueTitle = res.match[2]
    if res.match[3] is undefined
      issueDescription = ""
    else
      issueDescription = res.match[3]
      robot.http("https://api.gitter.im/v1/rooms/#{res.message.room}")
            .header('Content-Type', 'application/json')
            .header('Accept', 'application/json')
            .header('Authorization', "Bearer #{gitterToken}")
            .get() (err, response, body) ->
              roomURI = JSON.parse(body)["uri"]
              issueDescription += "\n\nOpened via [gitter](https://gitter.im/#{roomURI}/?at=#{res.message.id}) by @#{res.message.user.login}"

              if GITLAB_PROJECT_IDS.hasOwnProperty(repoName)
                robot.http("https://gitlab.com/api/v3/projects/#{GITLAB_PROJECT_IDS[repoName]}/issues?title=#{issueTitle}&description=#{issueDescription}")
                      .header("PRIVATE-TOKEN", "#{gitlabAccessToken}")
                      .post() (err, response, body) ->
                        sendResponse err, response, body, "gitlab"
              else
                data = {
                  "title": issueTitle,
                  "body": issueDescription
                }
                robot.http("https://api.github.com/repos/#{username}/#{repoName}/issues")
                      .header('Content-Type', 'application/json')
                      .header('Authorization', "token #{githubAccessToken}")
                      .post(JSON.stringify(data)) (err, response, body) ->
                        sendResponse err, response, body, "github"

    sendResponse = (err, response, body, source) ->
      if response.statusCode is not 201 or err
        if err is not undefined
          res.send "Issue filing failed :( Error:  #{err}"
        else
          res.send "Issue filing failed :( Status Code: #{statusCode}"
          return
      else
        linkToIssue = if source == "github" then JSON.parse(body)["html_url"] else JSON.parse(body)["web_url"]
        res.send "Here you go : #{linkToIssue}"
        return
