# Description:
#   Allows users to review pull requests without needing write access to the repos.
#
# Commands:
#   hubot mark (wip|pending) <pull URL> - Marks the given pull request as work in progress/pending review.
#
# Configuration:
#   HUBOT_GITHUB_OAUTH
#
# Author:
#   Lasse Schuirmann (@sils)

gh_token = process.env.HUBOT_GITHUB_OAUTH

module.exports = (robot) ->
  robot.respond /mark (wip|pending) (https:\/\/github.com\/coala\/([^\/]+)\/pull\/(\d+))/i, (msg) ->
    action = msg.match[1]
    pullUrl = msg.match[2]
    repo = msg.match[3]
    pullNumber = msg.match[4]
    if action is "wip"
      label = "process/wip"
      remLabel = "process/pending review"
      message = "The pull request #{pullUrl} is marked *work in progress*. Use `cobot pull needreview` or push to your branch if feedback from the community is needed again."
    else
      label = "process/pending review"
      remLabel = "process/wip"
      message = "The pull request #{pullUrl} is marked *pending review*, so you will get feedback from the community. Use `cobot pull wip` if there are known issues that should be corrected by the author."

    robot.http("https://api.github.com/repos/coala/#{repo}/issues/#{pullNumber}/labels")
      .header('Authorization', "token #{gh_token}")
      .post(JSON.stringify([label])) (err, res, body) ->
        if res.statusCode is not 200 or err
          if err is not undefined
            msg.send "Labelling failed :( #{err}"
          else
            msg.send "Labelling failed :( #{res.statusCode}"
        else
          robot.http("https://api.github.com/repos/coala/#{repo}/issues/#{pullNumber}/labels/#{remLabel}")
            .header('Authorization', "token #{gh_token}")
            .delete() (err, res, body) ->
              if res.statusCode is not 200 or err
                if err is not undefined
                  msg.send "Unlabelling failed :( #{err}"
                else
                  msg.send "Unlabelling failed :( #{res.statusCode}"
              else
                msg.send "#{message}"
